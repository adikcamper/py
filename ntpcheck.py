#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 13:32:13 2020

@author: adikcamper
"""

import ntplib
import time

nt = ntplib.NTPClient()

ntResponse = nt.request('pl.pool.ntp.org', version=3)

time1 = ntResponse.tx_time
time2 = time.time()

delta = time1-time2

if delta < -1 or delta > 1:
    print('NOK')
else:
    print('OK')

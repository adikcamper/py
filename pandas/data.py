#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 17:15:34 2020

@author: adikcamper
"""

import pandas as pd
# import matplotlib.pyplot as plt
import pandas_profiling


df = pd.read_csv("/home/adik/dane/py/pandas/data.csv",
                 sep=";",
                 names=['czas', 'wilgotnosc', 'temperatura'],
                 index_col='czas'
                 )

# df = pd.read_csv("/home/adik/dane/python/pandas/data.csv",
#                  sep=";",
#                  names=['czas', 'wilgotnosc', 'temperatura']
#                  )


df = df[df.temperatura != 'None']


df.index = pd.to_datetime(df.index)
df.temperatura.astype(float)
df.wilgotnosc.astype(float)

# %%
profile = pandas_profiling.ProfileReport(df)
profile.to_file('report.html')

# %%

# df.plot.line() # działa gdy czas jest indexem

# plt.plot(df.index, df.temperatura)
# plt.show()

# fig, (ax_temp, ax_humid) = plt.subplots(2)
# fig.suptitle('Temperatura i Wilgotność')
# ax_temp(df.index, df.temperatura)
# ax_humid(df.index, df.wilgotnosc)
# fig.show()

# print(df.columns)
# print(df.describe())

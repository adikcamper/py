#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 20:38:19 2020

@author: adikcamper
"""

import os
import sys

path = str(sys.argv[1])

print(path)

for dirpath, _, files in os.walk(path):
    for f in files:
        print(dirpath + '/' + f)

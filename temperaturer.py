#!/usr/bin/python3

import Adafruit_DHT
from datetime import datetime
from time import sleep
# import os

CSV_FILE='/home/pi/uzytki/temperaturer/data.csv'
DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4

while True:
    envdata=Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    with open(CSV_FILE, "a") as f:
        f.write(now + ';' + str(envdata[0]) + ';' + str(envdata[1]) +'\n')
        
    sleep(300)
    
